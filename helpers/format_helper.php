<?php

//simple format date function
function formatdate($date){
	return date('F j, Y, g:i a', strtotime($date));
}

//limit text block to 200 chars
function shortext($text , $chars = 200){
	$text = $text."";
	$text = substr($text, 0, $chars);
	$text = substr($text, 0, strrpos($text,' '));
	$text = $text."...";
	return $text;
}