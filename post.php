<?php include 'includes/header.php'; ?>
<?php
  //init db class  
  $db=new Database;
  //get id from URI
  $id=$_GET['id'];
  //get current post by id
  $query = "SELECT * FROM posts WHERE id =".$id;
  $post = $db->select($query)->fetch_assoc();
  //all categories
  $query = "SELECT * FROM categories";
  $categories = $db->select($query);
?>
<!-- show current post by id -->
  <div class="blog-post">
    <h2 class="blog-post-title"><?php echo $post['title']; ?></h2>
    <p class="blog-post-meta"><?php echo formatdate($post['date']); ?> by <a href="#"><?php echo $post['author']; ?></a></p>
      <?php echo $post['body']; ?>
  </div>

<?php include 'includes/footer.php'; ?>