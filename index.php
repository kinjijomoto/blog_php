
<!--blog php app --> 
<?php include 'includes/header.php'; ?>
<?php
//db class initialise 
  $db=new Database;
//select all posts 
  $query = "SELECT * FROM posts ORDER BY posts.date DESC";
//set it to variable  
  $posts = $db->select($query);
//select all categories
  $query = "SELECT * FROM categories";
  $categories = $db->select($query);
?>
<!-- put all posts on main page -->
<?php if($posts) : ?>
  <!-- put into array row all data from posts -->
    <?php while($row = $posts->fetch_assoc()) : ?>
          <div class="blog-post">
            <h2 class="blog-post-title"><?php echo $row['title']; ?></h2>
            <p class="blog-post-meta"><?php echo formatdate($row['date']); ?> by <a href="#"><?php echo $row['author']; ?></a></p>
              <?php echo shortext($row['body']); ?>
              <a class="readmore" href="post.php?id=<?php echo $row['id']; ?>">Read more</a>
          </div>
    <?php endwhile; ?>
<?php else : ?> No posts yet
<?php endif; ?>

<?php include 'includes/footer.php'; ?>