<?php include 'includes/header.php'; ?>
<?php
  //init db  
  $db=new Database;
  //get category num
  if(isset($_GET['category'])){
    $category = $_GET['category'];
    //select posts for current category
    $query = "SELECT * FROM posts WHERE category=".$category;
    $posts = $db->select($query);
  }else{
    //select all posts
    $query = "SELECT * FROM posts";
    $posts = $db->select($query);
  }
  //all categories
  $query = "SELECT * FROM categories";
  $categories = $db->select($query);
?>
<!-- show posts -->
<?php if($posts) : ?>
    <?php while($row = $posts->fetch_assoc()) : ?>
          <div class="blog-post">
            <h2 class="blog-post-title"><?php echo $row['title']; ?></h2>
            <p class="blog-post-meta"><?php echo formatdate($row['date']); ?> by <a href="#"><?php echo $row['author']; ?></a></p>
              <?php echo shortext($row['body']); ?>
              <a class="readmore" href="post.php?id=<?php echo $row['id']; ?>">Read more</a>
          </div>
    <?php endwhile; ?>
<?php else : ?> 
  <p>No posts yet</p>
<?php endif; ?>

<?php include 'includes/footer.php'; ?>