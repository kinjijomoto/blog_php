
</div>
<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
  <div class="sidebar-module sidebar-module-inset">
      <h4>About</h4>
      <!-- sidebar -->
      <p><?php echo $about; ?></p>
  </div>
<div class="sidebar-module">
  <h4>Cats</h4>
    <?php if($categories) : ?>
        <ol class="list-unstyled">
    <?php while($row = $categories->fetch_assoc()) : ?>
        <li><a href="posts.php?category=<?php echo $row['id']; ?>"><?php echo $row['name']; ?></a></li>
    <?php endwhile ?>
        </ol>
    <?php else : ?> 
      <p>no categories here</p>
    <?php endif; ?>
  </div>
  </div>
</div>
</div>
    <footer class="blog-footer">
      <p>Blog app, copyright 2015</p>
      <p>
        <a href="#">Back to top</a>
      </p>
    </footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
  </body>
</html>
