<?php include 'includes/header.php'; ?>
<?php
  //initialise db class
  $db = new Database;
  //select all posts
  $query="SELECT posts.*, categories.name FROM posts INNER JOIN categories ON categories.id = posts.category ORDER BY posts.date DESC";
  //all posts into var
  $posts=$db->select($query);
  //all categories into var
  $query = "SELECT * FROM categories";
  $categories = $db->select($query);
?>

<table class="table table-striped">
  <tr>
    <th>Post ID</th>
    <th>Post Title</th>
    <th>Post Category</th>
    <th>Post Author</th>
    <th>DATE</th>
  </tr>
    <?php while($row=$posts->fetch_assoc()) : ?>
        <tr>
          <td><?php echo $row['id']; ?> </td>
          <td><a href="edit_post.php?id=<?php echo $row['id']; ?>"> <?php echo $row['title']; ?> </a> </td>
          <td><?php echo $row['name']; ?> </td>
          <td><?php echo $row['author']; ?> </td>
          <td><?php echo formatdate($row['date']); ?> </td>
        </tr>
    <?php endwhile ?>
</table>

<table class="table table-striped">
  <tr>
    <th>Category ID</th>
    <th>Category </th>
  </tr>
    <?php while($row=$categories->fetch_assoc()) : ?>
        <tr>
          <td><?php echo $row['id']; ?> </td>
          <td><a href="edit_category.php?id=<?php echo $row['id']; ?>"> <?php echo $row['name']; ?> </a> </td>
        </tr>
    <?php endwhile ?>
</table>


<?php include 'includes/footer.php'; ?>

