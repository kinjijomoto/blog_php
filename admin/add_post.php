<?php include 'includes/header.php'; ?>
<?php
  //init db
  $db=new Database;
  //check submit button pressed
  if (isset($_POST['submit'])) {
    //data to vars
    $category= mysqli_real_escape_string($db->link, $_POST['category']);
    $title= mysqli_real_escape_string($db->link, $_POST['title']);
    $body= mysqli_real_escape_string($db->link, $_POST['body']);
    $tags= mysqli_real_escape_string($db->link, $_POST['tags']);
    $author= mysqli_real_escape_string($db->link, $_POST['author']);
    //Create query
    $query = "INSERT INTO posts (title, body, category, author, tags) 
        VALUES('$title', '$body', $category, '$author', '$tags')";
    //Run query            
    $insert_row = $db->insert($query);
	}
  //select all categories
  $query = "SELECT * FROM categories";
  $categories = $db->select($query);
?>
<form role="form" method="post" action="add_post.php">
  <div class="form-group">
    <label>Title</label>
    <input name="title" type="text" class="form-control"   placeholder="Enter Title">
  </div>
  <div class="form-group">
    <label>Body</label>
    <textarea name="body" class="form-control" placeholder="Enter body"> </textarea>
  </div>
  <div class="form-group">
   	<label>Category</label>
    <!-- load categories into option list -->
    <select name="category" class="form-control">
      <?php while($row=$categories->fetch_assoc()) : ?>
  		  <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?> </option>
  		<?php endwhile; ?>
	</select>
  </div>
  <div class="form-group">
    <label>Author</label>
    <input name="author" type="text" class="form-control"   placeholder="Enter name">
  </div>
  <div class="form-group">
    <label>Tags</label>
    <input name="tags" type="text" class="form-control"   placeholder="Enter tags">
  </div>
  <br>
  <div class="btn-forms">
    <input name="submit" type="submit" class="btn btn-default" value="Submit">
    <a href="index.php" class="btn btn-default">Cancel</a>
  </div>
  <br>
</form>

<?php include 'includes/footer.php'; ?>         