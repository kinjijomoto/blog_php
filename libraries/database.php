<?php 

//database class
class Database{
	public $host = 	   DB_HOST;
	public $username = DB_USER;
	public $password = DB_PASS;
	public $db_name =  DB_NAME;

	public $link;
	public $error;
//class construct 
	public function __construct(){
		$this->connect();
	}
//connect to db
	private function connect(){
		$this->link = new mysqli($this->host, $this->username, $this->password, $this->db_name);

		if(!$this->link){
			$this->error = "connection failed".$this->link->connect_error;
			return false;
		}
	}
//select from db
	public function select($query){
		//getting result from db or die(close page) with message
		$result = $this->link->query($query) or die($this->link->error.__LINE__);
		//if db have any row return result
		if($result->num_rows>0){
			return $result;
		}else{
			return false;
		}
	}
//insert into db
	public function insert($query){
		//almost same
		$insert_row = $this->link->query($query) or die($this->link->error.__LINE__);
		//check any row selected
		if($insert_row){
			header('Location: index.php?msg='.urlencode('Record Added'));
			exit();
		}else{
			die('Error:'.$this->link->errno.$this->link-error);
		}
	}
//uodate db
	public function update($query){
		$update_row = $this->link->query($query) or die($this->link->error.__LINE__);
		//check any row selected for update
		if($update_row){
			header('Location: index.php?msg='.urlencode('Record Updated'));
			exit();
		}else{
			die('Error:'.$this->link->errno.$this->link-error);
		}
	}
//delete any data from db
	public function delete($query){
		$delete_row = $this->link->query($query) or die($this->link->error.__LINE__);
		//check any row selected
		if($delete_row){
			header('Location: index.php?msg='.urlencode('Record Deleted'));
			exit();
		}else{
			die('Error:'.$this->link->errno.$this->link-error);
		}
	}
}
